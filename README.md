# faceit stats bot

## installation
python -m venv ~/.virtualenvs/faceitstatsbot

source ~/.virtualenvs/faceitstatsbot/bin/activate

pip install -r requirements.txt

## setup

* add faceit API key and discord bot token to settings.py
* add the id of the discord channel where commands are used settings.py

## run
python faceit_stats_bot.py &
