import discord
import logging
import settings
import sys


from discord.ext import commands
from faceit_stats import FaceItStats

logging.basicConfig(format='%(asctime)s %(levelname)-8s %(message)s', filemode='w', filename='faceitstatsbot.log',
                    level=logging.INFO)


faceit_stats = FaceItStats(api_key=settings.FACEIT_API_KEY)


intents = discord.Intents.default()
intents.members = True
intents.presences = True
bot = commands.Bot(command_prefix='!', intents=intents)


@bot.command()
async def stats(ctx, player, num_of_matches=2000):
        author = ctx.message.author
        stats = faceit_stats.get_stats(player, num_of_matches)
        if stats:
            logging.info(f'Sending FACEIT stats for user {author}')
            await ctx.send(embed=discord.Embed.from_dict(stats))


@bot.event
async def on_message(message):
    channel = message.channel
    if channel.id == settings.CS_TEXT_CHANNEL_ID:
        await bot.process_commands(message)


@bot.event
async def on_command(ctx):
    logging.info(f'{ctx.message.author} used command !{ctx.command}')


@bot.event
async def on_command_error(ctx, error):
    logging.error(f'{ctx.message.author}, {error}')


@bot.event
async def on_error(event, *args, **kwargs):
    logging.error(sys.exc_info())


@bot.event
async def on_ready():
    logging.info(f'Bot ready, username: {bot.user.name}, id: {bot.user.id}')


logging.info('Starting bot')
bot.run(settings.TOKEN)
