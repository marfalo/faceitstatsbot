import requests
import json
from datetime import datetime

class FaceItStats:
    def __init__(self, api_key):
        self.api_key = api_key

    def get_player_data(self, user):
        response = requests.get(
            'https://open.faceit.com/data/v4/players',
            params = {'nickname': user, 'game': 'csgo'},
            headers = {'Authorization': 'Bearer ' + self.api_key},
            )
        if response.status_code == 200:     
            data = json.loads(response.content)
            return data
        return None
    
    def get_stats(self, user, num_matches=2000):
        data = self.get_player_data(user)

        if not data or not num_matches:
            return None
        
        player_id = data['player_id']
        response = requests.get(
            f'https://api.faceit.com/stats/api/v1/stats/time/users/{player_id}/games/csgo',
            params = {'size': num_matches + 1}
            )
        if response.status_code == 200:     
            matches = json.loads(response.content)
            for match in list(matches):
                if not set(('c2', 'c4', 'i6', 'i10', 'date', 'elo')) <= match.keys():
                    matches.remove(match)

            kd_ratio = 0
            avg_kills = 0
            hs_rate = 0
            win_rate = 0

            if len(matches):
                if len(matches) < abs(num_matches):
                    num_matches = len(matches)
                    dif = -25 if int(matches[num_matches-1]['i10']) == 1 else 25
                    elo_start = int(matches[num_matches-1]['elo']) + dif
                else:
                    elo_start = int(matches[num_matches]['elo'])

                elo_end = int(matches[0]['elo'])
                elo_difference = elo_end - elo_start

                if num_matches < 0:
                    matches = matches[num_matches:]
                else:
                    matches = matches[:num_matches]

                for match in matches:
                    kd_ratio = kd_ratio + float(match['c2'])
                    avg_kills = avg_kills + int(match['i6'])
                    hs_rate = hs_rate + float(match['c4'])
                    win_rate = win_rate + int(match['i10'])

                date_start = datetime.fromtimestamp(matches[-1]['date'] / 1000).strftime('%d.%m.%Y')
                date_end = datetime.fromtimestamp(matches[0]['date'] / 1000).strftime('%d.%m.%Y')
                kd_ratio = round(kd_ratio / len(matches), 2)
                avg_kills = round(avg_kills / len(matches), 2)
                hs_rate = round(hs_rate / len(matches), 2)
                win_rate = round(win_rate / len(matches) * 100, 2)
                return {'fields': [{'inline': False, 'name': 'K/D ratio (avg)', 'value': str(kd_ratio)},
                       {'inline': False, 'name': 'Kills per match (avg)', 'value': str(avg_kills)},
                       {'inline': False, 'name': 'Headshot rate (%)', 'value': str(hs_rate)},
                       {'inline': False, 'name': 'Win rate (%)', 'value': str(win_rate)},
                       {'inline': False, 'name': 'ELO (start/end/difference)', 'value': f'{elo_start}/{elo_end}/{elo_difference}'}],
                       'type': 'rich', 'description':'', 'title': f'FACEIT stats from {date_start} to {date_end} ({len(matches)}) matches for player {user}'}
        return None
