"""
Settings module for faceit-stats bot
"""
# Tokens and API keys
TOKEN = None # Discord bot token
FACEIT_API_KEY = None # https://developers.faceit.com/docs/auth/api-keys

# Discord channel IDs where bot commands can be used 
CS_TEXT_CHANNEL_ID = None
